package com.dianxun.bookmanager.common.base;

import android.app.Activity;
import android.app.Fragment;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;


import com.dianxun.bookmanager.common.ActivityManager;
import com.dianxun.bookmanager.utils.ToastUtil;

import butterknife.ButterKnife;

/**
 * base activity
 */
public abstract class BaseActivity extends AppCompatActivity {

    protected Activity self;
    protected ActivityManager activityManager;
    public Context mContext;

    protected abstract int getContentView();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        self = this;
        mContext = this;
        if (getContentView() != 0) {
            setContentView(getContentView());
            ButterKnife.bind(this);
        }
        activityManager = ActivityManager.getScreenManager();
        activityManager.pushActivity(this);
    }

    protected final <T extends View> T findView(int id) {
        return (T) super.findViewById(id);
    }

    @Override
    protected void onNewIntent(Intent intent) {
        super.onNewIntent(intent);
        activityManager = ActivityManager.getScreenManager();
        activityManager.pushActivity(this);
    }

    @Override
    protected void onDestroy() {
        activityManager.popActivity(this);
        super.onDestroy();
    }

    @Override
    public void onAttachFragment(Fragment fragment) {
        super.onAttachFragment(fragment);
    }

    /**
     * 方法名称：jumpActivity
     * 方法描述：启动相关的界面. 不带bundle
     */
    public void jumpActivity(Class<? extends Activity> toActivity) {
        this.jumpActivity(toActivity, null);
    }

    /**
     * 方法名称：jumpActivity
     * 方法描述：启动相关的界面. 带bundle
     */
    public void jumpActivity(Class<? extends Activity> toActivity, Bundle bundle) {
        Intent intent = new Intent(mContext, toActivity);
        if (null != bundle) {
            intent.putExtras(bundle);
        }
        startActivity(intent);
    }

    /**
     * 跳转回调，用onActivityResultWithoutLogin接收,不带bundle
     *
     * @param reqcode
     * @param toActivity
     */
    public void jumpActivityForResult(int reqcode, Class<? extends Activity> toActivity) {
        this.jumpActivityForResult(reqcode, toActivity, null);
    }

    /**
     * 跳转回调，用onActivityResultWithoutLogin接收,带bundle
     *
     * @param reqcode
     * @param toActivity
     * @param bundle
     */
    public void jumpActivityForResult(int reqcode, Class<? extends Activity> toActivity, Bundle bundle) {
        Intent intent = new Intent();
        intent.setClass(mContext, toActivity);
        if (null != bundle) {
            intent.putExtras(bundle);
        }
        startActivityForResult(intent, reqcode);
    }

    public void toast(String msg) {
        ToastUtil.toast(msg);
    }
}