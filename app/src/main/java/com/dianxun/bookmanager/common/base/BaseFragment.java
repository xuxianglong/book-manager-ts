package com.dianxun.bookmanager.common.base;

import android.app.Activity;
import android.content.Intent;
import android.nfc.NfcAdapter;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;

import com.dianxun.bookmanager.utils.ToastUtil;

import java.lang.reflect.Field;

import butterknife.ButterKnife;

public abstract class BaseFragment extends Fragment {

	public Activity mContext;
	private View contentView;

	/**
	 * 返回布局显示ID
	 * @return 布局id
	 */
	protected abstract int getContentView();

	/**
	 * 初始化View（findView替代findViewById）
	 */
	protected abstract void initView();

	/**
	 * 初始化监听
	 */
	protected abstract void initListeners();

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		mContext = getActivity();
		contentView = inflater.inflate(getContentView(), null);
		ButterKnife.bind(this, contentView);
		LinearLayout.LayoutParams lp = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.MATCH_PARENT);
		contentView.setLayoutParams(lp);
		initView();
		initListeners();
		return contentView;
	}

	@SuppressWarnings("unchecked")
	protected final <T extends View> T findView(int id) {
		return (T) contentView.findViewById(id);
	}

	public View getContentRootView(){
		return contentView;
	}
	/**
	 * 方法名称：jumpActivity
	 * 方法描述：启动相关的界面. 不带bundle
	 */
	public void jumpActivity(Class<? extends Activity> toActivity) {
		this.jumpActivity(toActivity, null);

	}
	/**
	 * 方法名称：jumpActivity
	 * 方法描述：启动相关的界面. 带bundle
	 */
	public void jumpActivity(Class<? extends Activity> toActivity, Bundle bundle) {
		Intent intent = new Intent(mContext, toActivity);
		if (null != bundle) {
			intent.putExtras(bundle);
		}
		startActivity(intent);
	}
	/**
	 * 跳转回调，用onActivityResultWithoutLogin接收,不带bundle
	 * @param reqcode
	 * @param toActivity
	 */
	public void jumpActivityForResult(int reqcode, Class<? extends Activity> toActivity) {
		this.jumpActivityForResult(reqcode, toActivity, null);

	}

	/**
	 * 跳转回调，用onActivityResultWithoutLogin接收,带bundle
	 * @param reqcode
	 * @param toActivity
	 * @param bundle
	 */
	public void jumpActivityForResult(int reqcode, Class<? extends Activity> toActivity, Bundle bundle) {
		Intent intent = new Intent();
		intent.setClass(mContext, toActivity);
		if (null != bundle) {
			intent.putExtras(bundle);
		}
		startActivityForResult(intent, reqcode);
	}

	public void toast(String msg) {
		ToastUtil.toast(msg);
	}

	@Override
	public void onDestroy() {
		super.onDestroy();
	}

	// http://stackoverflow.com/questions/15207305/getting-the-error-java-lang-illegalstateexception-activity-has-been-destroyed
	@Override
	public void onDetach() {
		super.onDetach();
		try {
			Field childFragmentManager = Fragment.class.getDeclaredField("mChildFragmentManager");
			childFragmentManager.setAccessible(true);
			childFragmentManager.set(this, null);
		} catch (NoSuchFieldException e) {
			throw new RuntimeException(e);
		} catch (IllegalAccessException e) {
			throw new RuntimeException(e);
		}
	}

	public boolean checkNFC() {
		NfcAdapter nfcAdapter = NfcAdapter.getDefaultAdapter(mContext);
		if (nfcAdapter == null) {
			toast("设备不支持NFC！");
			return false;
		}
		if (!nfcAdapter.isEnabled()) {
			toast("请在系统设置中先启用NFC功能！");
			return false;
		}
		return true;
	}
}
