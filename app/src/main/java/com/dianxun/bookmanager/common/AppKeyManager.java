package com.dianxun.bookmanager.common;

/**
 * 缓存管理Kye或者全局静态常量
 * @author liyushen
 */
public class AppKeyManager {

    public static String SP_USERID = "userid";
    public static String SP_LOGINNAME = "loginname";
    public static String SP_USERNAME = "username";
    public static String SP_USERTYPE = "usertype";
    // 1：正常 2：逾期
    public static final int TYPE_READING_NORMAL = 1;
    public static final int TYPE_READING_OVERDUE = 2;
    // 在库标志位：flag（在库传1/不在库传0/全部传空）
    public static final int TYPE_BOOKLIB_IN = 1;
    public static final int TYPE_BOOKLIB_OUT = 0;

    public static final String EXTRA_BOOKID = "bookId";
    public static final String EXTRA_ENTITY = "entity";
    public static final int REQUEST_USERLIST = 1001;
}
