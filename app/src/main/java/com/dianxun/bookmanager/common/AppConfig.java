package com.dianxun.bookmanager.common;

import android.content.Context;
import android.content.SharedPreferences;

import com.dianxun.bookmanager.MainApplication;
import com.dianxun.bookmanager.utils.FileUtil;
import com.yanzhenjie.nohttp.tools.IOUtils;

import java.io.File;

/**
 * app配置
 */
public class AppConfig {

    private static AppConfig appConfig;

    private SharedPreferences preferences;

    /**
     * App根目录.
     */
    public String APP_PATH_ROOT;

    private AppConfig() {
        preferences = MainApplication.getInstance().getSharedPreferences("BOOK", Context.MODE_PRIVATE);

        APP_PATH_ROOT = FileUtil.getRootPath(MainApplication.getInstance()).getAbsolutePath() + File.separator +
                "GYXC";
    }

    public static AppConfig getInstance() {
        if (appConfig == null)
            synchronized (AppConfig.class) {
                if (appConfig == null)
                    appConfig = new AppConfig();
            }
        return appConfig;
    }

    public void initialize() {
        IOUtils.createFolder(APP_PATH_ROOT);
    }

    public void putInt(String key, int value) {
        preferences.edit().putInt(key, value).commit();
    }

    public int getInt(String key, int defValue) {
        return preferences.getInt(key, defValue);
    }

    public void putString(String key, String value) {
        preferences.edit().putString(key, value).commit();
    }

    public String getString(String key) {
        return preferences.getString(key, "");
    }

    public String getString(String key, String defValue) {
        return preferences.getString(key, defValue);
    }

    public void putBoolean(String key, boolean value) {
        preferences.edit().putBoolean(key, value).commit();
    }

    public boolean getBoolean(String key, boolean defValue) {
        return preferences.getBoolean(key, defValue);
    }

    public void putLong(String key, long value) {
        preferences.edit().putLong(key, value).commit();
    }

    public long getLong(String key, long defValue) {
        return preferences.getLong(key, defValue);
    }

    public void putFloat(String key, float value) {
        preferences.edit().putFloat(key, value).commit();
    }

    public float getFloat(String key, float defValue) {
        return preferences.getFloat(key, defValue);
    }

    public void putDouble(String key, double value) {
        preferences.edit().putFloat(key, (float) value).commit();
    }

    public double getDouble(String key, double defValue) {
        return preferences.getFloat(key, (float) defValue);
    }

    public String getUserId() {
        return getString(AppKeyManager.SP_USERID, "");
    }

    public void setUserId(String userId) {
        putString(AppKeyManager.SP_USERID, userId);
    }

    public String getUsername() {
        return getString(AppKeyManager.SP_USERNAME, "");
    }

    public void setUsername(String username) {
        putString(AppKeyManager.SP_USERNAME, username);
    }

    public String getLoginname() {
        return getString(AppKeyManager.SP_LOGINNAME, "");
    }

    public void setLoginname(String loginname) {
        putString(AppKeyManager.SP_LOGINNAME, loginname);
    }

    public String getUserType() {
        return getString(AppKeyManager.SP_USERTYPE, "0");
    }

    public void setUserType(String userType) {
        putString(AppKeyManager.SP_USERTYPE, userType);
    }
}
