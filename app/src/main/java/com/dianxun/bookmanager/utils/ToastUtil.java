package com.dianxun.bookmanager.utils;

import android.support.annotation.StringRes;
import android.view.Gravity;
import android.widget.Toast;

import com.dianxun.bookmanager.MainApplication;


/**
 * Created by liyushen on 2017/10/30.
 * 功能说明：
 */
public class ToastUtil {

    private static Toast toast;

    public static void toast(String textStr) {
        if (textStr == null) {
            return;
        }
        toast = Toast.makeText(MainApplication.getInstance(), textStr, Toast.LENGTH_SHORT);
        toast.setGravity(Gravity.CENTER, 0, 0);
        try {
            toast.show();
        } catch (Exception e) {

        }
    }

    public static void toast(@StringRes int stringId) {
        toast = Toast.makeText(MainApplication.getInstance(), stringId, Toast.LENGTH_SHORT);
        toast.setGravity(Gravity.CENTER, 0, 0);
        try {
            toast.show();
        } catch (Exception e) {

        }
    }
}
