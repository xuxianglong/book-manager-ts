package com.dianxun.bookmanager.ui.activity;

import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

import com.dianxun.bookmanager.R;
import com.dianxun.bookmanager.common.AppConfig;
import com.dianxun.bookmanager.common.base.BaseActivity;
import com.dianxun.bookmanager.entity.LoginEntity;
import com.dianxun.bookmanager.entity.ServiceResult;
import com.dianxun.bookmanager.net.Api;
import com.dianxun.bookmanager.net.HttpListener;

import butterknife.BindView;

/**
 * 登录
 */
public class LoginActivity extends BaseActivity implements View.OnClickListener {

    @BindView(R.id.et_user)
    EditText et_user;
    @BindView(R.id.et_password)
    EditText et_password;
    @BindView(R.id.btn_login)
    Button btn_login;

    @Override
    protected int getContentView() {
        return R.layout.activity_login;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        // 测试数据
        et_user.setText(AppConfig.getInstance().getLoginname());
//        et_password.setText("123");
        btn_login.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.btn_login:
                final String loginname = et_user.getText().toString().trim();
                String password = et_password.getText().toString().trim();
                if (TextUtils.isEmpty(loginname)) {
                    toast("请输入用户名");
                    return;
                }
                if (TextUtils.isEmpty(password)) {
                    toast("请输入密码");
                    return;
                }
                Api.login(mContext, loginname, password, new HttpListener<ServiceResult>() {
                    @Override
                    public void onSucceed(ServiceResult response) {
                        if (response.getCode()!= null && response.getCode().equals("1")) {
                            LoginEntity.UserInfoBean entity = ((LoginEntity) response).getUserInfo();
                            AppConfig.getInstance().setUserId(entity.getUserId());
                            AppConfig.getInstance().setUsername(entity.getUserName());
                            String userType = entity.getUserType();
                            AppConfig.getInstance().setUserType(userType);
                            if (userType.equals("0")) {
                                AppConfig.getInstance().setLoginname(loginname);
                                jumpActivity(MainManagerActivity.class);
                                finish();
                            } else {
                                toast("非管理员，无法登录");
                            }
                        } else {
                            toast(response.getMessage());
                        }
                    }

                    @Override
                    public void onFailed() {

                    }
                }, LoginEntity.class);
                break;
        }
    }
}
