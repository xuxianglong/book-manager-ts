package com.dianxun.bookmanager.ui.activity;

import android.support.annotation.NonNull;
import android.support.design.widget.BottomNavigationView;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.ViewPager;
import android.os.Bundle;
import android.view.MenuItem;
import android.widget.TextView;

import com.dianxun.bookmanager.R;
import com.dianxun.bookmanager.common.base.BaseActivity;
import com.dianxun.bookmanager.ui.fragment.BookLibFragment;
import com.dianxun.bookmanager.ui.fragment.OverdueFragment;
import com.dianxun.bookmanager.ui.fragment.ReadingFragment;
import com.dianxun.bookmanager.ui.widget.NoScrollViewPager;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;

public class MainUserActivity extends BaseActivity {

    @BindView(R.id.tv_title)
    TextView tv_title;
    @BindView(R.id.viewpager)
    NoScrollViewPager mViewPager;
    @BindView(R.id.bottom_navigation)
    BottomNavigationView bottomNavigationView;
    private FragmentPagerAdapter fragmentPagerAdapter;
    private List<Fragment> mFragment = new ArrayList<>(3);
    ReadingFragment readingFragment;
    OverdueFragment overdueFragment;
    BookLibFragment bookLibFragment;

    @Override
    protected int getContentView() {
        return R.layout.activity_main_user;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        fragmentPagerAdapter = new FragmentPagerAdapter(getSupportFragmentManager()) {
            @Override
            public Fragment getItem(int position) {
                return mFragment.get(position);
            }

            @Override
            public int getCount() {
                return mFragment.size();
            }

            @Override
            public CharSequence getPageTitle(int position) {
                switch (position) {
                    case 0:
                        return getString(R.string.reading);
                    case 1:
                        return getString(R.string.overdue);
                    case 2:
                        return getString(R.string.book_libing);
                }
                return super.getPageTitle(position);
            }
        };

        bottomNavigationView.setOnNavigationItemSelectedListener(
                new BottomNavigationView.OnNavigationItemSelectedListener() {
                    @Override
                    public boolean onNavigationItemSelected(@NonNull MenuItem item) {
                        switch (item.getItemId()) {
                            case R.id.item_reading:
                                mViewPager.setCurrentItem(0, false);
                                tv_title.setText(fragmentPagerAdapter.getPageTitle(0));
                                return true;
                            case R.id.item_overdue:
                                mViewPager.setCurrentItem(1, false);
                                tv_title.setText(fragmentPagerAdapter.getPageTitle(1));
                                return true;
                            case R.id.item_book_lib:
                                mViewPager.setCurrentItem(2, false);
                                tv_title.setText(fragmentPagerAdapter.getPageTitle(2));
                                return true;
                        }
                        return false;
                    }
                });

        readingFragment = new ReadingFragment();
        mFragment.add(readingFragment);
        overdueFragment = new OverdueFragment();
        mFragment.add(overdueFragment);
        bookLibFragment = new BookLibFragment();
        mFragment.add(bookLibFragment);
        mViewPager.setAdapter(fragmentPagerAdapter);
        mViewPager.setOffscreenPageLimit(3);
        mViewPager.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {/*empty*/}

            @Override
            public void onPageSelected(int position) {
                tv_title.setText(fragmentPagerAdapter.getPageTitle(position));
            }

            @Override
            public void onPageScrollStateChanged(int state) {
            }
        });
    }
}
