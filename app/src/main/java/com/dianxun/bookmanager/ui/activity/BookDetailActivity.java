package com.dianxun.bookmanager.ui.activity;

import android.graphics.Color;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.chad.library.adapter.base.BaseQuickAdapter;
import com.chad.library.adapter.base.BaseViewHolder;
import com.dianxun.bookmanager.R;
import com.dianxun.bookmanager.adapter.FlowAdapter;
import com.dianxun.bookmanager.adapter.FlowDetailAdapter;
import com.dianxun.bookmanager.common.AppKeyManager;
import com.dianxun.bookmanager.common.base.BaseActivity;
import com.dianxun.bookmanager.entity.BookDetailEntity;
import com.dianxun.bookmanager.entity.OverdueEntity;
import com.dianxun.bookmanager.entity.ServiceResult;
import com.dianxun.bookmanager.net.Api;
import com.dianxun.bookmanager.net.HttpListener;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;

/**
 * 图书详情
 */
public class BookDetailActivity extends BaseActivity implements View.OnClickListener  {

    @BindView(R.id.iv_back)
    ImageView iv_back;
    @BindView(R.id.iv_flag)
    ImageView iv_flag;
    @BindView(R.id.tv_book_detail)
    TextView tv_book_detail;
    @BindView(R.id.tv_book_status)
    TextView tv_book_status;
    @BindView(R.id.tv_book_content)
    TextView tv_book_content;
    @BindView(R.id.list_data)
    RecyclerView list_data;
    FlowDetailAdapter adapter;

    @Override
    protected int getContentView() {
        return R.layout.activity_book_detail;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        iv_back.setOnClickListener(this);

        list_data.setLayoutManager(new LinearLayoutManager(mContext));
        list_data.setNestedScrollingEnabled(false);

        Api.getBookInfoById(self, getIntent().getExtras().getString(AppKeyManager.EXTRA_BOOKID),
                new HttpListener<ServiceResult>() {
                    @Override
                    public void onSucceed(ServiceResult response) {
                        if (response.getCode()!= null && response.getCode().equals("1")) {
                            BookDetailEntity.DataInfoBean entity = ((BookDetailEntity) response).getDataInfo();

                            tv_book_detail.setText("书名：" + entity.getBookName() + "\n"
                                + "作者：" + entity.getAuthor() + "\n"
                                    + "出版社：" + entity.getPress() + "\n"
                                    + "出版日期：" + entity.getPublishDate());
                            tv_book_content.setText(entity.getContent());
                            if (entity.getFlag() == AppKeyManager.TYPE_BOOKLIB_IN) {
                                tv_book_status.setText("在库");
                                tv_book_status.setBackgroundColor(Color.parseColor("#e1f3e1"));
                                tv_book_status.setTextColor(getResources().getColor(R.color.green));
                                iv_flag.setImageResource(R.drawable.lib_in);
                            } else {
                                if (entity.getExpectReturnDayNum() >= 0) {
                                    tv_book_status.setText("已借出/预计" + entity.getExpectReturnDayNum()+ "天后归还");
                                } else {
                                    tv_book_status.setText("已借出/逾期" + Math.abs(entity.getExpectReturnDayNum()) + "天");
                                }
                                tv_book_status.setBackgroundColor(Color.parseColor("#ffe1e1"));
                                tv_book_status.setTextColor(getResources().getColor(R.color.red));
                                iv_flag.setImageResource(R.drawable.lib_out);
                            }
                            adapter = new FlowDetailAdapter();
                            View headView = getLayoutInflater().inflate(R.layout.layout_detail_flow_header, (ViewGroup) list_data.getParent(), false);
                            adapter.addHeaderView(headView);
                            list_data.setAdapter(adapter);
                            adapter.setNewData(entity.getRecordDetailList());
                        } else {
                            toast(response.getMessage());
                            finish();
                        }
                    }

                    @Override
                    public void onFailed() {
                        finish();
                    }
                }, BookDetailEntity.class);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.iv_back:
                finish();
                break;
        }
    }
}
