package com.dianxun.bookmanager.ui.fragment;

import android.app.DatePickerDialog;
import android.os.Bundle;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.view.ViewGroup;
import android.widget.DatePicker;

import com.chad.library.adapter.base.BaseQuickAdapter;
import com.chad.library.adapter.base.BaseViewHolder;
import com.dianxun.bookmanager.R;
import com.dianxun.bookmanager.common.AppConfig;
import com.dianxun.bookmanager.common.AppKeyManager;
import com.dianxun.bookmanager.common.base.BaseFragment;
import com.dianxun.bookmanager.entity.OverdueEntity;
import com.dianxun.bookmanager.entity.ServiceResult;
import com.dianxun.bookmanager.net.Api;
import com.dianxun.bookmanager.net.HttpListener;
import com.dianxun.bookmanager.ui.activity.BookDetailActivity;
import com.dianxun.bookmanager.utils.Utils;

import java.util.Calendar;

import butterknife.BindView;

/**
 * 逾期未还
 */
public class OverdueFragment extends BaseFragment {

    @BindView(R.id.refreshLayout)
    SwipeRefreshLayout refreshLayout;
    @BindView(R.id.list_data)
    RecyclerView list_data;
    private OverdueAdapter adapter;

    @Override
    protected int getContentView() {
        return R.layout.fragment_overdue;
    }

    @Override
    protected void initView() {
        refreshLayout.setColorSchemeResources(android.R.color.holo_blue_light, android.R.color.holo_red_light,
                android.R.color.holo_orange_light, android.R.color.holo_green_light);
        refreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                refreshDatas();
            }
        });

        list_data.setLayoutManager(new LinearLayoutManager(mContext));
        adapter = new OverdueAdapter();
        View headView = getLayoutInflater().inflate(R.layout.layout_overdue_header, (ViewGroup) list_data.getParent(), false);
        adapter.addHeaderView(headView);
        adapter.setOnItemClickListener(new BaseQuickAdapter.OnItemClickListener() {
            @Override
            public void onItemClick(BaseQuickAdapter adapter, View view, int position) {
                OverdueEntity.DataListBean entity = (OverdueEntity.DataListBean) adapter.getData().get(position);
                Bundle b = new Bundle();
                b.putString(AppKeyManager.EXTRA_BOOKID, entity.getBookId());
                jumpActivity(BookDetailActivity.class, b);
            }
        });
        if (AppConfig.getInstance().getUserType().equals("0")) {
            adapter.setOnItemLongClickListener(new BaseQuickAdapter.OnItemLongClickListener() {
                @Override
                public boolean onItemLongClick(BaseQuickAdapter adapter, View view, int position) {
                    final OverdueEntity.DataListBean entity = (OverdueEntity.DataListBean) adapter.getData().get(position);
                    toast("请选择续借日期");
                    final Calendar calendar = Calendar.getInstance();
                    DatePickerDialog datePicker = new DatePickerDialog(mContext, new DatePickerDialog.OnDateSetListener() {

                        @Override
                        public void onDateSet(DatePicker view, int year, int monthOfYear,
                                              int dayOfMonth) {
                            calendar.set(year, monthOfYear, dayOfMonth);
                            Api.renewBook(mContext, entity.getRecordId(), Utils.getTimeString(calendar.getTimeInMillis()), new HttpListener<ServiceResult>() {
                                @Override
                                public void onSucceed(ServiceResult response) {
                                    if (response.getCode()!= null && response.getCode().equals("1")) {
                                        toast("续借成功");
                                        refreshDatas();
                                    } else {
                                        toast(response.getMessage());
                                    }
                                }

                                @Override
                                public void onFailed() {

                                }
                            }, ServiceResult.class);
                        }
                    }, calendar.get(Calendar.YEAR), calendar.get(Calendar.MONTH), calendar.get(Calendar.DAY_OF_MONTH));
                    datePicker.show();
                    return true;
                }
            });
        }
        list_data.setAdapter(adapter);

        refreshDatas();
    }

    private void refreshDatas() {
        refreshLayout.setRefreshing(true);
        if (AppConfig.getInstance().getUserType().equals("0")) {
            Api.getOutDayBookListAll(mContext, new HttpListener<ServiceResult>() {
                @Override
                public void onSucceed(ServiceResult response) {
                    refreshLayout.setRefreshing(false);
                    if (response.getCode()!= null && response.getCode().equals("1")) {
                        OverdueEntity entity = (OverdueEntity) response;
                        adapter.setNewData(entity.getDataList());
                    } else {
                        toast(response.getMessage());
                    }
                }

                @Override
                public void onFailed() {
                    refreshLayout.setRefreshing(false);
                }
            }, OverdueEntity.class);
        } else {
            Api.getOutDayBookList(mContext, new HttpListener<ServiceResult>() {
                @Override
                public void onSucceed(ServiceResult response) {
                    refreshLayout.setRefreshing(false);

                    if (response.getCode()!= null && response.getCode().equals("1")) {
                        OverdueEntity entity = (OverdueEntity) response;
                        adapter.setNewData(entity.getDataList());
                    } else {
                        toast(response.getMessage());
                    }
                }

                @Override
                public void onFailed() {
                    refreshLayout.setRefreshing(false);
                }
            }, OverdueEntity.class);
        }
    }

    @Override
    protected void initListeners() {

    }

    public class OverdueAdapter extends BaseQuickAdapter<OverdueEntity.DataListBean, BaseViewHolder> {

        public OverdueAdapter() {
            super(R.layout.layout_overdue_item, null);
        }

        @Override
        protected void convert(BaseViewHolder helper, OverdueEntity.DataListBean item) {
            helper.setText(R.id.tv_bookname, item.getBookName());
            helper.setText(R.id.tv_expectreturndate, item.getExpectReturnDate());
            helper.setText(R.id.tv_expectreturndaynum, item.getExpectReturnDayNum() + "天");
        }
    }
}
