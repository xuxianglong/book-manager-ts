package com.dianxun.bookmanager.ui.activity.sort;

import com.dianxun.bookmanager.entity.UserListEntity;

import java.util.Comparator;

public class PinyinComparator implements Comparator<UserListEntity.UserListBean> {

	public int compare(UserListEntity.UserListBean o1, UserListEntity.UserListBean o2) {
		if (o1.getLetters().equals("@")
				|| o2.getLetters().equals("#")) {
			return -1;
		} else if (o1.getLetters().equals("#")
				|| o2.getLetters().equals("@")) {
			return 1;
		} else {
			return o1.getLetters().compareTo(o2.getLetters());
		}
	}
}
