package com.dianxun.bookmanager.ui.fragment;

import android.os.Bundle;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;

import com.chad.library.adapter.base.BaseQuickAdapter;
import com.dianxun.bookmanager.R;
import com.dianxun.bookmanager.adapter.ReadingAdapter;
import com.dianxun.bookmanager.common.AppKeyManager;
import com.dianxun.bookmanager.common.base.BaseFragment;
import com.dianxun.bookmanager.entity.ReadBookListEntity;
import com.dianxun.bookmanager.entity.ReadingEntity;
import com.dianxun.bookmanager.entity.ServiceResult;
import com.dianxun.bookmanager.net.Api;
import com.dianxun.bookmanager.net.HttpListener;
import com.dianxun.bookmanager.ui.activity.BookDetailActivity;

import java.util.List;

import butterknife.BindView;

/**
 * 阅读中
 */
public class ReadingFragment extends BaseFragment {

    @BindView(R.id.refreshLayout)
    SwipeRefreshLayout refreshLayout;
    @BindView(R.id.list_data)
    RecyclerView list_data;
    private ReadingAdapter adapter;

    @Override
    protected int getContentView() {
        return R.layout.fragment_reading;
    }

    @Override
    protected void initView() {
        refreshLayout.setColorSchemeResources(android.R.color.holo_blue_light, android.R.color.holo_red_light,
                android.R.color.holo_orange_light, android.R.color.holo_green_light);
        refreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                refreshDatas();
            }
        });

        list_data.setLayoutManager(new LinearLayoutManager(mContext));

        refreshDatas();
    }

    private void refreshDatas() {
        refreshLayout.setRefreshing(true);
        Api.getReadBookList(mContext, new HttpListener<ServiceResult>() {
            @Override
            public void onSucceed(ServiceResult response) {
                refreshLayout.setRefreshing(false);
                if (response.getCode()!= null && response.getCode().equals("1")) {
                    ReadBookListEntity entity = (ReadBookListEntity) response;
                    List<ReadingEntity> datas = entity.getDataList();
                    for (int i = 0; i < datas.size(); i++) {
                        ReadingEntity bean = datas.get(i);
                        if (bean.getExpectReturnDayNum() >= 0) {
                            bean.setType(AppKeyManager.TYPE_READING_NORMAL);
                        } else {
                            bean.setType(AppKeyManager.TYPE_READING_OVERDUE);
                        }
                        datas.set(i, bean);
                    }
                    adapter = new ReadingAdapter(datas);
                    adapter.setHasStableIds(true);
                    adapter.setOnItemClickListener(new BaseQuickAdapter.OnItemClickListener() {
                        @Override
                        public void onItemClick(BaseQuickAdapter adapter, View view, int position) {
                            ReadingEntity entity = (ReadingEntity) adapter.getData().get(position);
                            Bundle b = new Bundle();
                            b.putString(AppKeyManager.EXTRA_BOOKID, entity.getBookId());
                            jumpActivity(BookDetailActivity.class, b);
                        }
                    });
                    list_data.setAdapter(adapter);
                } else {
                    toast(response.getMessage());
                }
            }

            @Override
            public void onFailed() {
                refreshLayout.setRefreshing(false);
            }
        }, ReadBookListEntity.class);
    }

    @Override
    protected void initListeners() {

    }
}
