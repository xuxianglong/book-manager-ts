package com.dianxun.bookmanager.ui.fragment;

import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.nfc.NfcAdapter;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.TextView;

import com.dianxun.bookmanager.R;
import com.dianxun.bookmanager.adapter.FlowAdapter;
import com.dianxun.bookmanager.common.AppKeyManager;
import com.dianxun.bookmanager.common.base.BaseFragment;
import com.dianxun.bookmanager.entity.BookDetailEntity;
import com.dianxun.bookmanager.entity.ServiceResult;
import com.dianxun.bookmanager.net.Api;
import com.dianxun.bookmanager.net.HttpListener;
import com.dianxun.bookmanager.utils.Utils;
import com.google.zxing.integration.android.IntentIntegrator;

import java.util.ArrayList;

import butterknife.BindView;

/**
 * 归还
 */
public class RetFragment extends BaseFragment implements View.OnClickListener {

    @BindView(R.id.tv_scan)
    TextView tv_scan;
    @BindView(R.id.tv_rfid)
    TextView tv_rfid;
    @BindView(R.id.tv_book_detail)
    TextView tv_book_detail;
    @BindView(R.id.list_data)
    RecyclerView list_data;
    @BindView(R.id.btn_ret)
    TextView btn_ret;
    FlowAdapter adapter;
    private String bookId;
    public ProgressDialog progressDialog;

    @Override
    protected int getContentView() {
        return R.layout.fragment_ret;
    }

    @Override
    protected void initView() {
        list_data.setLayoutManager(new LinearLayoutManager(mContext));
        list_data.setNestedScrollingEnabled(false);
    }

    @Override
    protected void initListeners() {
        tv_scan.setOnClickListener(this);
        tv_rfid.setOnClickListener(this);
        btn_ret.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.tv_scan:
                if (Utils.getSystemModel().equals("PDA")) {
                    final AlertDialog alertDialog = new AlertDialog.Builder(mContext).setTitle("请扫描条码")
                            .setNegativeButton("取消", null).create();
                    LayoutInflater factory = LayoutInflater.from(mContext);
                    final View view = factory.inflate(R.layout.layout_scan_edit, null);
                    final EditText edittext = view.findViewById(R.id.editText);
                    edittext.setOnTouchListener(new View.OnTouchListener() {
                        @Override
                        public boolean onTouch(View v, MotionEvent event) {
                            return true;
                        }
                    });
                    edittext.addTextChangedListener(new TextWatcher() {
                        @Override
                        public void beforeTextChanged(CharSequence s, int start, int count, int after) {

                        }

                        @Override
                        public void onTextChanged(CharSequence s, int start, int before, int count) {

                        }

                        @Override
                        public void afterTextChanged(Editable s) {
                            String barNo = edittext.getText().toString();
                            getBookInfoByBarNo(barNo);
                            if (alertDialog != null && alertDialog.isShowing()) {
                                alertDialog.cancel();
                            }
                        }
                    });
                    alertDialog.setView(view);
                    alertDialog.show();
                } else {
                    IntentIntegrator integrator = new IntentIntegrator(getActivity());
                    integrator.setOrientationLocked(false);
                    integrator.initiateScan();
                }
                break;
            case R.id.tv_rfid:
                if (checkNFC()) {
                    progressDialog = new ProgressDialog(mContext);
                    progressDialog.setTitle("请读取RFID标签");
                    progressDialog.show();
                }
                break;
            case R.id.btn_ret:
                if (TextUtils.isEmpty(bookId)) {
                    toast("请扫描图书");
                    return;
                }
                Api.returnBook(mContext, bookId, new HttpListener<ServiceResult>() {
                    @Override
                    public void onSucceed(ServiceResult response) {
                        if (response.getCode() != null && response.getCode().equals("1")) {
                            toast("还书成功");
                            bookId = null;
                            tv_book_detail.setText("");
                            adapter.setNewData(new ArrayList<BookDetailEntity.RecordDetailListBean>());
                        } else {
                            toast(response.getMessage());
                        }
                    }

                    @Override
                    public void onFailed() {

                    }
                }, ServiceResult.class);
                break;
        }
    }

    public void getBookInfoByRfidNo(String rfidNo) {
        if (progressDialog != null && progressDialog.isShowing()) {
            progressDialog.dismiss();
            progressDialog = null;
        }
        Api.getBookInfoByRfidNo(mContext, rfidNo, new HttpListener<ServiceResult>() {
            @Override
            public void onSucceed(ServiceResult response) {
                if (response.getCode() != null && response.getCode().equals("1")) {
                    BookDetailEntity.DataInfoBean entity = ((BookDetailEntity) response).getDataInfo();
                    tv_book_detail.setText("书名：" + entity.getBookName() + "\n"
                            + "作者：" + entity.getAuthor() + "\n"
                            + "出版社：" + entity.getPress() + "\n"
                            + "出版日期：" + entity.getPublishDate());
                    bookId = entity.getBookId();
                    if (adapter == null) {
                        adapter = new FlowAdapter();
                        View headView = getLayoutInflater().inflate(R.layout.layout_flow_header, (ViewGroup) list_data.getParent(), false);
                        adapter.addHeaderView(headView);
                        list_data.setAdapter(adapter);
                    }
                    adapter.setNewData(entity.getRecordDetailList());
                    if (entity.getFlag() == AppKeyManager.TYPE_BOOKLIB_IN) {
                        toast("该图书有库存，请核查！");
                        return;
                    }
                } else {
                    toast(response.getMessage());
                }
            }

            @Override
            public void onFailed() {

            }
        }, BookDetailEntity.class);
    }

    public void getBookInfoByBarNo(String barNo) {
        //"T00004"
        Api.getBookInfoByBarNo(mContext, barNo, new HttpListener<ServiceResult>() {
            @Override
            public void onSucceed(ServiceResult response) {
                if (response.getCode() != null && response.getCode().equals("1")) {
                    BookDetailEntity.DataInfoBean entity = ((BookDetailEntity) response).getDataInfo();
                    tv_book_detail.setText("书名：" + entity.getBookName() + "\n"
                            + "作者：" + entity.getAuthor() + "\n"
                            + "出版社：" + entity.getPress() + "\n"
                            + "出版日期：" + entity.getPublishDate());
                    if (adapter == null) {
                        adapter = new FlowAdapter();
                        View headView = getLayoutInflater().inflate(R.layout.layout_flow_header, (ViewGroup) list_data.getParent(), false);
                        adapter.addHeaderView(headView);
                        list_data.setAdapter(adapter);
                    }
                    adapter.setNewData(entity.getRecordDetailList());
                    bookId = entity.getBookId();
                    if (entity.getFlag() == AppKeyManager.TYPE_BOOKLIB_IN) {
                        toast("该图书有库存，请核查！");
                        return;
                    }
                } else {
                    toast(response.getMessage());
                }
            }

            @Override
            public void onFailed() {

            }
        }, BookDetailEntity.class);
    }

}
