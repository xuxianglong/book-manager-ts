package com.dianxun.bookmanager.ui.fragment;

import android.os.Bundle;
import android.support.v7.widget.AppCompatSpinner;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.KeyEvent;
import android.view.View;
import android.view.inputmethod.EditorInfo;
import android.widget.AdapterView;
import android.widget.EditText;
import android.widget.TextView;

import com.chad.library.adapter.base.BaseQuickAdapter;
import com.chad.library.adapter.base.BaseViewHolder;
import com.dianxun.bookmanager.R;
import com.dianxun.bookmanager.common.AppKeyManager;
import com.dianxun.bookmanager.common.base.BaseFragment;
import com.dianxun.bookmanager.entity.BookLibEntity;
import com.dianxun.bookmanager.entity.ServiceResult;
import com.dianxun.bookmanager.net.Api;
import com.dianxun.bookmanager.net.HttpListener;
import com.dianxun.bookmanager.ui.activity.BookDetailActivity;
import com.dianxun.bookmanager.utils.RxKeyboardTool;

import java.util.List;

import butterknife.BindView;

/**
 * 图书库
 */
public class BookLibFragment extends BaseFragment {

    @BindView(R.id.sp_show_type)
    AppCompatSpinner sp_show_type;
    @BindView(R.id.et_search)
    EditText et_search;
    @BindView(R.id.list_data)
    RecyclerView list_data;
    private BookLibAdapter adapter;
    private int flag;
    private int pageindex = -1;
    private boolean isLastPage = false;
    private String appSearchName = null;

    @Override
    protected int getContentView() {
        return R.layout.fragment_book_lib;
    }

    @Override
    protected void initView() {
        list_data.setLayoutManager(new LinearLayoutManager(mContext));

        sp_show_type.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                switch (position) {
                    case 0:
                        flag = -1;
                        refreshDatas(et_search.getText().toString().trim());
                        RxKeyboardTool.hideSoftInput(mContext);
                        break;
                    case 1:
                        flag = AppKeyManager.TYPE_BOOKLIB_IN;
                        refreshDatas(et_search.getText().toString().trim());
                        RxKeyboardTool.hideSoftInput(mContext);
                        break;
                    case 2:
                        flag = AppKeyManager.TYPE_BOOKLIB_OUT;
                        refreshDatas(et_search.getText().toString().trim());
                        RxKeyboardTool.hideSoftInput(mContext);
                        break;
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        et_search.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                if(actionId == EditorInfo.IME_ACTION_SEARCH) {
                    refreshDatas(et_search.getText().toString().trim());
                    RxKeyboardTool.hideSoftInput(mContext);
                }
                return false;
            }
        });

        adapter = new BookLibAdapter();
        adapter.setOnItemClickListener(new BaseQuickAdapter.OnItemClickListener() {
            @Override
            public void onItemClick(BaseQuickAdapter adapter, View view, int position) {
                BookLibEntity.DataListBean entity = (BookLibEntity.DataListBean) adapter.getData().get(position);
                Bundle b = new Bundle();
                b.putString(AppKeyManager.EXTRA_BOOKID, entity.getBookId());
                jumpActivity(BookDetailActivity.class, b);
            }
        });
        adapter.setHasStableIds(true);
        list_data.setAdapter(adapter);

        refreshDatas("");
    }

    public void refreshDatas(String appSearchName) {
        pageindex = 0;
        isLastPage = false;
        this.appSearchName = appSearchName;
        getDatas();
    }

    public void getDatas() {
        if (isLastPage) {
            adapter.loadMoreEnd();
            return;
        }
        Api.getBookList(mContext, pageindex, appSearchName, flag, new HttpListener<ServiceResult>() {
            @Override
            public void onSucceed(ServiceResult response) {
                if (response.getCode()!= null && response.getCode().equals("1")) {
                    List<BookLibEntity.DataListBean> list = ((BookLibEntity) response).getDataList();
                    if (pageindex == 0) {
                        adapter.setNewData(list);
                        adapter.loadMoreComplete();
                    } else {
                        adapter.addData(list);
                        adapter.loadMoreComplete();
                    }
                    if (list.size() < 20) {
                        isLastPage = true;
                        adapter.loadMoreEnd();
                    } else {
                        pageindex++;
                    }
                } else {
                    toast(response.getMessage());
                }
            }

            @Override
            public void onFailed() {

            }
        }, BookLibEntity.class);
    }

    @Override
    protected void initListeners() {

    }

    public class BookLibAdapter extends BaseQuickAdapter<BookLibEntity.DataListBean, BaseViewHolder> {

        public BookLibAdapter() {
            super(R.layout.item_booklib, null);
        }

        @Override
        protected void convert(BaseViewHolder helper, BookLibEntity.DataListBean item) {
            if (item.getFlag() == AppKeyManager.TYPE_BOOKLIB_IN) {
                helper.setGone(R.id.layout_lib_in, true);
                helper.setGone(R.id.layout_lib_out, false);
                helper.setText(R.id.tv_bookname_in, item.getBookName());
                helper.setText(R.id.tv_booktypename_in, item.getBookTypeName());
                helper.setText(R.id.tv_author_in, item.getAuthor());
                helper.setText(R.id.tv_publishdate_in, item.getPublishDate());
                helper.setText(R.id.tv_lenddate_in, item.getLendDate());
                helper.setText(R.id.tv_expectreturndate_in,  item.getExpectReturnDate());
            } else {
                helper.setGone(R.id.layout_lib_in, false);
                helper.setGone(R.id.layout_lib_out, true);
                helper.setText(R.id.tv_bookname_out, item.getBookName());
                helper.setText(R.id.tv_booktypename_out, item.getBookTypeName());
                helper.setText(R.id.tv_author_out, item.getAuthor());
                helper.setText(R.id.tv_publishdate_out, item.getPublishDate());
                helper.setText(R.id.tv_lenddate_out, item.getLendDate());
                helper.setText(R.id.tv_expectreturndate_out,  item.getExpectReturnDate());
            }
        }
    }
}
