package com.dianxun.bookmanager.ui.activity;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.view.View;
import android.widget.TextView;

import com.dianxun.bookmanager.R;
import com.dianxun.bookmanager.common.AppKeyManager;
import com.dianxun.bookmanager.common.base.BaseActivity;
import com.dianxun.bookmanager.entity.UserListEntity;
import com.dianxun.bookmanager.ui.activity.sort.ClearEditText;
import com.dianxun.bookmanager.ui.activity.sort.PinyinComparator;
import com.dianxun.bookmanager.ui.activity.sort.PinyinUtils;
import com.dianxun.bookmanager.ui.activity.sort.SideBar;
import com.dianxun.bookmanager.ui.activity.sort.SortAdapter;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class UserListActivity extends BaseActivity {

    private RecyclerView mRecyclerView;
    private SideBar sideBar;
    private TextView dialog;
    private SortAdapter adapter;
    private ClearEditText mClearEditText;
    LinearLayoutManager manager;

    private List<UserListEntity.UserListBean> SourceDateList;

    /**
     * 根据拼音来排列RecyclerView里面的数据类
     */
    private PinyinComparator pinyinComparator;

    @Override
    protected int getContentView() {
        return R.layout.activity_user_list;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        SourceDateList = filledData(((UserListEntity) getIntent().getExtras().getSerializable(AppKeyManager.EXTRA_ENTITY)).getUserList());

        initViews();
    }

    private void initViews() {
        pinyinComparator = new PinyinComparator();

        sideBar = findViewById(R.id.sideBar);
        dialog = findViewById(R.id.dialog);
        sideBar.setTextView(dialog);

        //设置右侧SideBar触摸监听
        sideBar.setOnTouchingLetterChangedListener(new SideBar.OnTouchingLetterChangedListener() {

            @Override
            public void onTouchingLetterChanged(String s) {
                //该字母首次出现的位置
                int position = adapter.getPositionForSection(s.charAt(0));
                if (position != -1) {
                    manager.scrollToPositionWithOffset(position, 0);
                }

            }
        });

        mRecyclerView = findViewById(R.id.recyclerView);

        // 根据a-z进行排序源数据
        Collections.sort(SourceDateList, pinyinComparator);
        //RecyclerView社置manager
        manager = new LinearLayoutManager(this);
        manager.setOrientation(LinearLayoutManager.VERTICAL);
        mRecyclerView.setLayoutManager(manager);
        adapter = new SortAdapter(this, SourceDateList);
        mRecyclerView.setAdapter(adapter);
        adapter.setOnItemClickListener(new SortAdapter.OnItemClickListener() {
            @Override
            public void onItemClick(View view, int position) {
                UserListEntity.UserListBean entity = (UserListEntity.UserListBean) adapter.getItem(position);
                Intent data = new Intent();
                data.putExtra(AppKeyManager.EXTRA_ENTITY, entity);
                setResult(RESULT_OK, data);
                finish();
            }
        });
        mClearEditText = (ClearEditText) findViewById(R.id.filter_edit);

        //根据输入框输入值的改变来过滤搜索
        mClearEditText.addTextChangedListener(new TextWatcher() {

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                //当输入框里面的值为空，更新为原来的列表，否则为过滤数据列表
                filterData(s.toString());
            }

            @Override
            public void beforeTextChanged(CharSequence s, int start, int count,
                                          int after) {

            }

            @Override
            public void afterTextChanged(Editable s) {
            }
        });
    }


    /**
     * 为RecyclerView填充数据
     *
     * @param datas
     * @return
     */
    private List<UserListEntity.UserListBean> filledData(List<UserListEntity.UserListBean> datas) {
        List<UserListEntity.UserListBean> mSortList = new ArrayList<>();

        for (int i = 0; i < datas.size(); i++) {
            UserListEntity.UserListBean sortModel = new UserListEntity.UserListBean();
            sortModel.setUserName(datas.get(i).getUserName());
            sortModel.setUserId(datas.get(i).getUserId());
            sortModel.setSex(datas.get(i).getSex());
            //汉字转换成拼音
            String pinyin = PinyinUtils.getPingYin(datas.get(i).getUserName());
            String sortString = pinyin.substring(0, 1).toUpperCase();

            // 正则表达式，判断首字母是否是英文字母
            if (sortString.matches("[A-Z]")) {
                sortModel.setLetters(sortString.toUpperCase());
            } else {
                sortModel.setLetters("#");
            }

            mSortList.add(sortModel);
        }
        return mSortList;

    }

    /**
     * 根据输入框中的值来过滤数据并更新RecyclerView
     *
     * @param filterStr
     */
    private void filterData(String filterStr) {
        List<UserListEntity.UserListBean> filterDateList = new ArrayList<>();

        if (TextUtils.isEmpty(filterStr)) {
            filterDateList = SourceDateList;
        } else {
            filterDateList.clear();
            for (UserListEntity.UserListBean sortModel : SourceDateList) {
                String name = sortModel.getUserName();
                if (name.indexOf(filterStr.toString()) != -1 ||
                        PinyinUtils.getFirstSpell(name).startsWith(filterStr.toString())
                        //不区分大小写
                        || PinyinUtils.getFirstSpell(name).toLowerCase().startsWith(filterStr.toString())
                        || PinyinUtils.getFirstSpell(name).toUpperCase().startsWith(filterStr.toString())
                        ) {
                    filterDateList.add(sortModel);
                }
            }
        }

        // 根据a-z进行排序
        Collections.sort(filterDateList, pinyinComparator);
        adapter.updateList(filterDateList);
    }

}
