package com.dianxun.bookmanager.net;

import android.content.Context;
import android.text.TextUtils;

import com.dianxun.bookmanager.R;
import com.dianxun.bookmanager.common.AppConfig;
import com.dianxun.bookmanager.common.AppKeyManager;
import com.dianxun.bookmanager.entity.ServiceResult;

import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.util.HashMap;
import java.util.Map;

/**
 * 接口定义
 */
public class Api {

    public static final int what_readbooklist = 0x01;
    public static final int what_outdaybooklist = 0x02;
    public static final int what_booklist = 0x03;
    public static final int what_bookinfobyid = 0x04;
    public static final int what_bookinfobybarno = 0x05;
    public static final int what_bookinfobyrfidno = 0x06;
    public static final int what_savebookrecord = 0x07;
    public static final int what_returnbook = 0x08;
    public static final int what_outdaybooklistall = 0x09;
    public static final int what_renewbook = 0x10;
    public static final int what_login = 0x11;
    public static final int what_userlist = 0x12;

    /**
     * 根据登录用户获取-阅读中 图书集合
     *
     * @param context
     * @param netCallBack
     * @param rspCls
     */
    public static void getReadBookList(Context context, HttpListener<ServiceResult> netCallBack, Class<?> rspCls) {
        String url = "mapps.readBookList.get&userId=" + AppConfig.getInstance().getUserId();
        CallServer.getInstance().get(context, what_readbooklist, url, null, null, netCallBack, rspCls);
    }

    /**
     * 根据登录用户获取-逾期未还 图书集合
     *
     * @param context
     * @param netCallBack
     * @param rspCls
     */
    public static void getOutDayBookList(Context context, HttpListener<ServiceResult> netCallBack, Class<?> rspCls) {
        String url = "mapps.outDayBookList.get&userId=" + AppConfig.getInstance().getUserId();
        CallServer.getInstance().get(context, what_outdaybooklist, url, null, null, netCallBack, rspCls);
    }

    /**
     * 图书馆集合
     *
     * @param context
     * @param offset
     * @param appSearchName
     * @param flag
     * @param netCallBack
     * @param rspCls
     */
    public static void getBookList(Context context, int offset, String appSearchName, int flag, HttpListener<ServiceResult> netCallBack, Class<?> rspCls) {
        String url = "mapps.bookList.get";
        if (!TextUtils.isEmpty(appSearchName)) {
            url += "&appSearchName=" + appSearchName;
        }
        url += "&limit=20&offset=" + offset * 20;
        if (flag != -1) {
            url += "&flag=" + flag;
        }
        CallServer.getInstance().get(context, what_booklist, url, null, null, netCallBack, rspCls);
    }

    /**
     * 查看图书详情
     *
     * @param context
     * @param bookId
     * @param netCallBack
     * @param rspCls
     */
    public static void getBookInfoById(Context context, String bookId, HttpListener<ServiceResult> netCallBack, Class<?> rspCls) {
        String url = "mapps.bookInfoById.get&bookId=" + bookId;
        CallServer.getInstance().get(context, what_bookinfobyid, url, null, "正在读取信息", netCallBack, rspCls);
    }

    /**
     * 根据条形码获取图书信息
     *
     * @param context
     * @param barNo
     * @param netCallBack
     * @param rspCls
     */
    public static void getBookInfoByBarNo(Context context, String barNo, HttpListener<ServiceResult> netCallBack, Class<?> rspCls) {
        String url = "mapps.bookInfoByBarNo.get&barNo=" + barNo;
        CallServer.getInstance().get(context, what_bookinfobybarno, url, null, "正在读取信息", netCallBack, rspCls);
    }

    /**
     * 根据RFID获取图书信息
     *
     * @param context
     * @param rfidNo
     * @param netCallBack
     * @param rspCls
     */
    public static void getBookInfoByRfidNo(Context context, String rfidNo, HttpListener<ServiceResult> netCallBack, Class<?> rspCls) {
        String url = "mapps.bookInfoByRfidNo.get&rfidNo=" + rfidNo;
        CallServer.getInstance().get(context, what_bookinfobyrfidno, url, null, "正在读取信息", netCallBack, rspCls);
    }

    /**
     * 保存借书记录
     * @param context
     * @param bookId
     * @param userId
     * @param userName
     * @param lendDate
     * @param expectReturnDate
     * @param netCallBack
     * @param rspCls
     */
    public static void savebookRecord(Context context, String bookId, String userId, String userName,
                                      String lendDate, String expectReturnDate, HttpListener<ServiceResult> netCallBack, Class<?> rspCls) {
        String url = "mapps.bookRecord.save";
        Map<String, Object> params = new HashMap<>();
        params.put("bookId", bookId);
        params.put("userId", userId);
        params.put("userName", userName);
        params.put("lendDate", lendDate);
        params.put("expectReturnDate", expectReturnDate);
        CallServer.getInstance().post(context, what_savebookrecord, url, params, "保存还书记录", netCallBack, rspCls);
    }

    /**
     * 还书
     * @param context
     * @param bookId
     * @param netCallBack
     * @param rspCls
     */
    public static void returnBook(Context context, String bookId, HttpListener<ServiceResult> netCallBack, Class<?> rspCls) {
        String url = "mapps.returnBook.save";
        Map<String, Object> params = new HashMap<>();
        params.put("bookId", bookId);
        CallServer.getInstance().post(context, what_returnbook, url, params, "正在还书", netCallBack, rspCls);
    }

    /**
     * 逾期未还 图书集合
     * @param context
     * @param netCallBack
     * @param rspCls
     */
    public static void getOutDayBookListAll(Context context, HttpListener<ServiceResult> netCallBack, Class<?> rspCls) {
        String url = "mapps.outDayBookList.get";
        CallServer.getInstance().get(context, what_outdaybooklistall, url, null, null, netCallBack, rspCls);
    }

    /**
     * 续借
     * @param context
     * @param recordId
     * @param expectReturnDate
     * @param netCallBack
     * @param rspCls
     */
    public static void renewBook(Context context, String recordId, String expectReturnDate, HttpListener<ServiceResult> netCallBack, Class<?> rspCls) {
        String url = "mapps.renewBook.save";
        Map<String, Object> params = new HashMap<>();
        params.put("recordId", recordId);
        params.put("expectReturnDate", expectReturnDate);
        CallServer.getInstance().post(context, what_renewbook, url, params, "正在续借", netCallBack, rspCls);
    }

    /**
     * 登录
     * @param context
     * @param userName
     * @param password
     * @param netCallBack
     * @param rspCls
     */
    public static void login(Context context, String userName, String password, HttpListener<ServiceResult> netCallBack, Class<?> rspCls) {
        String url = "mapps.appUser.login&userName=" + userName + "&password=" + password;
        CallServer.getInstance().get(context, what_login, url, null, "正在登录", netCallBack, rspCls);
    }

    /**
     * 获取人员集合（字母表）
     * @param context
     * @param netCallBack
     * @param rspCls
     */
    public static void getUserList(Context context, HttpListener<ServiceResult> netCallBack, Class<?> rspCls) {
        String url = "mapps.userList.get";
        CallServer.getInstance().get(context, what_userlist, url, null, "正在获取用户列表", netCallBack, rspCls);
    }
}