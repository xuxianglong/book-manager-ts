package com.dianxun.bookmanager.net;

/**
 * http回调
 */
public interface HttpListener<T> {
    void onSucceed(T response);
    void onFailed();
}
