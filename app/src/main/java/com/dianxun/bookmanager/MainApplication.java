package com.dianxun.bookmanager;

import android.app.ActivityManager;
import android.app.Application;
import android.content.Context;

import com.yanzhenjie.nohttp.InitializationConfig;
import com.yanzhenjie.nohttp.NoHttp;
import com.yanzhenjie.nohttp.URLConnectionNetworkExecutor;
import com.yanzhenjie.nohttp.cache.DBCacheStore;
import com.yanzhenjie.nohttp.cookie.DBCookieStore;

/**
 * 应用入口
 */
public class MainApplication extends Application {

    private static MainApplication _instance;

    @Override
    public void onCreate() {
        super.onCreate();

        _instance = this;

        if (getApplicationInfo().packageName.equals(getCurProcessName(getApplicationContext()))) {
            InitializationConfig config = InitializationConfig.newBuilder(this)
                    // 全局连接服务器超时时间，单位毫秒，默认10s。
                    .connectionTimeout(8 * 1000)
                    // 全局等待服务器响应超时时间，单位毫秒，默认10s。
                    .readTimeout(8 * 1000)
                    // 配置缓存，默认保存数据库DBCacheStore，保存到SD卡使用DiskCacheStore。
                    .cacheStore(
                            // 如果不使用缓存，setEnable(false)禁用。
                            new DBCacheStore(this).setEnable(true)
                    )
                    // 配置Cookie，默认保存数据库DBCookieStore，开发者可以自己实现CookieStore接口。
                    .cookieStore(
                            // 如果不维护cookie，setEnable(false)禁用。
                            new DBCookieStore(this).setEnable(true)
                    )
                    // 配置网络层，默认URLConnectionNetworkExecutor，如果想用OkHttp：OkHttpNetworkExecutor。
                    .networkExecutor(new URLConnectionNetworkExecutor())
                    .build();
            NoHttp.initialize(config);
        }
    }

    public static MainApplication getInstance() {
        return _instance;
    }

    public static String getCurProcessName(Context context) {
        int pid = android.os.Process.myPid();
        ActivityManager activityManager = (ActivityManager) context.getSystemService(Context.ACTIVITY_SERVICE);
        for (ActivityManager.RunningAppProcessInfo appProcess : activityManager.getRunningAppProcesses()) {
            if (appProcess.pid == pid) {
                return appProcess.processName;
            }
        }
        return null;
    }
}
