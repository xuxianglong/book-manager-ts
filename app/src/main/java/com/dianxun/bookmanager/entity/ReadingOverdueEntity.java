package com.dianxun.bookmanager.entity;

/**
 * 阅读中（逾期未还）
 */
public class ReadingOverdueEntity extends TypeEntity {

    private String content;

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }
}
