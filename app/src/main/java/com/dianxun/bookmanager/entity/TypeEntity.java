package com.dianxun.bookmanager.entity;

/**
 * 类型entity
 */
public class TypeEntity {

    private int type;

    public int getType() {
        return type;
    }

    public void setType(int type) {
        this.type = type;
    }
}
