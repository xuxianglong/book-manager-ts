package com.dianxun.bookmanager.entity;

/***
 * 请求结果
 */
public class LoginEntity extends ServiceResult {

    /**
     * userInfo : {"userId":"1","userName":"管理员","userType":"0"}
     */

    private UserInfoBean userInfo;

    public UserInfoBean getUserInfo() {
        return userInfo;
    }

    public void setUserInfo(UserInfoBean userInfo) {
        this.userInfo = userInfo;
    }

    public static class UserInfoBean {
        /**
         * userId : 1
         * userName : 管理员
         * userType : 0
         */

        private String userId;
        private String userName;
        private String userType;

        public String getUserId() {
            return userId;
        }

        public void setUserId(String userId) {
            this.userId = userId;
        }

        public String getUserName() {
            return userName;
        }

        public void setUserName(String userName) {
            this.userName = userName;
        }

        public String getUserType() {
            return userType;
        }

        public void setUserType(String userType) {
            this.userType = userType;
        }
    }
}
