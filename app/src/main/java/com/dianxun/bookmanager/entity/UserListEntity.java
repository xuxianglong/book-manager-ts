package com.dianxun.bookmanager.entity;

import java.io.Serializable;
import java.util.List;

/***
 * 请求结果
 */
public class UserListEntity extends ServiceResult {

    private List<UserListBean> userList;

    public List<UserListBean> getUserList() {
        return userList;
    }

    public void setUserList(List<UserListBean> userList) {
        this.userList = userList;
    }

    public static class UserListBean implements Serializable {
        /**
         * userId : 8
         * userName : 测试员
         * sex : 男
         */

        private String userId;
        private String userName;
        private String sex;
        private String letters;//显示拼音的首字母

        public String getUserId() {
            return userId;
        }

        public void setUserId(String userId) {
            this.userId = userId;
        }

        public String getUserName() {
            return userName;
        }

        public void setUserName(String userName) {
            this.userName = userName;
        }

        public String getSex() {
            return sex;
        }

        public void setSex(String sex) {
            this.sex = sex;
        }

        public String getLetters() {
            return letters;
        }

        public void setLetters(String letters) {
            this.letters = letters;
        }
    }
}
