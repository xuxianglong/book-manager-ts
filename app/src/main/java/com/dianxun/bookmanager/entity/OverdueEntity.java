package com.dianxun.bookmanager.entity;

import java.util.List;

/***
 * 请求结果
 */
public class OverdueEntity extends ServiceResult {

    private List<DataListBean> dataList;

    public List<DataListBean> getDataList() {
        return dataList;
    }

    public void setDataList(List<DataListBean> dataList) {
        this.dataList = dataList;
    }

    public static class DataListBean {
        /**
         * recordId : 2c4f524acaf343598f5beeb9a1a4d6e5
         * bookId : b9290c8fcf7644278914c1635977de92
         * bookName : 水浒传
         * expectReturnDate : 2018-08-03
         * expectReturnDayNum : 84
         * flag : 0
         * lenderName : 用户一
         */

        private String recordId;
        private String bookId;
        private String bookName;
        private String expectReturnDate;
        private int expectReturnDayNum;
        private int flag;
        private String lenderName;

        public String getRecordId() {
            return recordId;
        }

        public void setRecordId(String recordId) {
            this.recordId = recordId;
        }

        public String getBookId() {
            return bookId;
        }

        public void setBookId(String bookId) {
            this.bookId = bookId;
        }

        public String getBookName() {
            return bookName;
        }

        public void setBookName(String bookName) {
            this.bookName = bookName;
        }

        public String getExpectReturnDate() {
            return expectReturnDate;
        }

        public void setExpectReturnDate(String expectReturnDate) {
            this.expectReturnDate = expectReturnDate;
        }

        public int getExpectReturnDayNum() {
            return expectReturnDayNum;
        }

        public void setExpectReturnDayNum(int expectReturnDayNum) {
            this.expectReturnDayNum = expectReturnDayNum;
        }

        public int getFlag() {
            return flag;
        }

        public void setFlag(int flag) {
            this.flag = flag;
        }

        public String getLenderName() {
            return lenderName;
        }

        public void setLenderName(String lenderName) {
            this.lenderName = lenderName;
        }
    }
}
