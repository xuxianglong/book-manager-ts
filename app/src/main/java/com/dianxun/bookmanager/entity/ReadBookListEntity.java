package com.dianxun.bookmanager.entity;

import java.util.List;

public class ReadBookListEntity extends ServiceResult {

    private List<ReadingEntity> dataList;

    public List<ReadingEntity> getDataList() {
        return dataList;
    }

    public void setDataList(List<ReadingEntity> dataList) {
        this.dataList = dataList;
    }
}
