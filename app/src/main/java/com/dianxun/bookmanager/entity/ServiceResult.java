package com.dianxun.bookmanager.entity;

import java.io.Serializable;

/***
 * 请求结果
 */
public class ServiceResult implements Serializable {

	private String code;
	private String message;

	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}
}
