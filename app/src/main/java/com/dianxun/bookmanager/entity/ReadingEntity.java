package com.dianxun.bookmanager.entity;

/**
 * 阅读中（正常）
 */
public class ReadingEntity extends TypeEntity {

    /**
     * bookId : b9290c8fcf7644278914c1635977de92
     * bookName : 水浒传
     * bookTypeName : 军事
     * author : 田七
     * press : 党建读物出版社
     * publishDate : 2018-05-07
     * lendDate : 2018-07-17
     * expectReturnDayNum : -84
     * readDayNum : 101
     * flag : 0
     */

    private String bookId;
    private String bookName;
    private String bookTypeName;
    private String author;
    private String press;
    private String publishDate;
    private String lendDate;
    private int expectReturnDayNum;
    private int readDayNum;
    private int flag;

    public String getBookId() {
        return bookId;
    }

    public void setBookId(String bookId) {
        this.bookId = bookId;
    }

    public String getBookName() {
        return bookName;
    }

    public void setBookName(String bookName) {
        this.bookName = bookName;
    }

    public String getBookTypeName() {
        return bookTypeName;
    }

    public void setBookTypeName(String bookTypeName) {
        this.bookTypeName = bookTypeName;
    }

    public String getAuthor() {
        return author;
    }

    public void setAuthor(String author) {
        this.author = author;
    }

    public String getPress() {
        return press;
    }

    public void setPress(String press) {
        this.press = press;
    }

    public String getPublishDate() {
        return publishDate;
    }

    public void setPublishDate(String publishDate) {
        this.publishDate = publishDate;
    }

    public String getLendDate() {
        return lendDate;
    }

    public void setLendDate(String lendDate) {
        this.lendDate = lendDate;
    }

    public int getExpectReturnDayNum() {
        return expectReturnDayNum;
    }

    public void setExpectReturnDayNum(int expectReturnDayNum) {
        this.expectReturnDayNum = expectReturnDayNum;
    }

    public int getReadDayNum() {
        return readDayNum;
    }

    public void setReadDayNum(int readDayNum) {
        this.readDayNum = readDayNum;
    }

    public int getFlag() {
        return flag;
    }

    public void setFlag(int flag) {
        this.flag = flag;
    }
}
