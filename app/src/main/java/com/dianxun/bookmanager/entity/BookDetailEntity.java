package com.dianxun.bookmanager.entity;

import java.util.List;

public class BookDetailEntity extends ServiceResult {

    /**
     * dataInfo : {"bookId":"b9290c8fcf7644278914c1635977de92","bookName":"水浒传","bookTypeName":"军事","author":"田七","press":"党建读物出版社","publishDate":"2018-05-07","expectReturnDayNum":-85,"flag":0,"content":"《马克思主义哲学史》出版单位：北京出版社主编：黄楠森等1996年12月第1版全书共分八卷，是我国\u201c七五\u201d计划中哲学社会科学国家科研重点项目之一，参与编写者共57人，都是活跃在马克思主义哲学史教学和科研第一线的哲学工作者。","recordDetailList":[{"userName":"用户一","lendDate":"2018-07-17","returnDate":""}]}
     */

    private DataInfoBean dataInfo;

    public DataInfoBean getDataInfo() {
        return dataInfo;
    }

    public void setDataInfo(DataInfoBean dataInfo) {
        this.dataInfo = dataInfo;
    }

    public static class DataInfoBean {
        /**
         * bookId : b9290c8fcf7644278914c1635977de92
         * bookName : 水浒传
         * bookTypeName : 军事
         * author : 田七
         * press : 党建读物出版社
         * publishDate : 2018-05-07
         * expectReturnDayNum : -85
         * flag : 0
         * content : 《马克思主义哲学史》出版单位：北京出版社主编：黄楠森等1996年12月第1版全书共分八卷，是我国“七五”计划中哲学社会科学国家科研重点项目之一，参与编写者共57人，都是活跃在马克思主义哲学史教学和科研第一线的哲学工作者。
         * recordDetailList : [{"userName":"用户一","lendDate":"2018-07-17","returnDate":""}]
         */

        private String bookId;
        private String bookName;
        private String bookTypeName;
        private String author;
        private String press;
        private String publishDate;
        private int expectReturnDayNum;
        private int flag;
        private String content;
        private List<RecordDetailListBean> recordDetailList;

        public String getBookId() {
            return bookId;
        }

        public void setBookId(String bookId) {
            this.bookId = bookId;
        }

        public String getBookName() {
            return bookName;
        }

        public void setBookName(String bookName) {
            this.bookName = bookName;
        }

        public String getBookTypeName() {
            return bookTypeName;
        }

        public void setBookTypeName(String bookTypeName) {
            this.bookTypeName = bookTypeName;
        }

        public String getAuthor() {
            return author;
        }

        public void setAuthor(String author) {
            this.author = author;
        }

        public String getPress() {
            return press;
        }

        public void setPress(String press) {
            this.press = press;
        }

        public String getPublishDate() {
            return publishDate;
        }

        public void setPublishDate(String publishDate) {
            this.publishDate = publishDate;
        }

        public int getExpectReturnDayNum() {
            return expectReturnDayNum;
        }

        public void setExpectReturnDayNum(int expectReturnDayNum) {
            this.expectReturnDayNum = expectReturnDayNum;
        }

        public int getFlag() {
            return flag;
        }

        public void setFlag(int flag) {
            this.flag = flag;
        }

        public String getContent() {
            return content;
        }

        public void setContent(String content) {
            this.content = content;
        }

        public List<RecordDetailListBean> getRecordDetailList() {
            return recordDetailList;
        }

        public void setRecordDetailList(List<RecordDetailListBean> recordDetailList) {
            this.recordDetailList = recordDetailList;
        }


    }

    public static class RecordDetailListBean {
        /**
         * userName : 用户一
         * lendDate : 2018-07-17
         * returnDate :
         */

        private String userName;
        private String lendDate;
        private String returnDate;
        private String expectReturnDate;

        public String getUserName() {
            return userName;
        }

        public void setUserName(String userName) {
            this.userName = userName;
        }

        public String getLendDate() {
            return lendDate;
        }

        public void setLendDate(String lendDate) {
            this.lendDate = lendDate;
        }

        public String getReturnDate() {
            return returnDate;
        }

        public void setReturnDate(String returnDate) {
            this.returnDate = returnDate;
        }

        public String getExpectReturnDate() {
            return expectReturnDate;
        }

        public void setExpectReturnDate(String expectReturnDate) {
            this.expectReturnDate = expectReturnDate;
        }
    }
}
