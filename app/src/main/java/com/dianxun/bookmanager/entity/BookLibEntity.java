package com.dianxun.bookmanager.entity;

import java.util.List;

public class BookLibEntity extends ServiceResult {

    /**
     * total : 8
     * dataList : [{"bookId":"52821ca7f6ac40b9b4663813b384632f","bookName":"西游记","bookTypeName":"农业","author":"李四","press":"安徽人民出版社","publishDate":"2018-07-17","lendDate":"2018-07-10","expectReturnDate":"2018-07-18","flag":0},{"bookId":"555e2ecce0544cbb9361bf2378926833","bookName":"人生如梦","bookTypeName":"科技","author":"这张三","press":"上海第一人民出版社","publishDate":"2013-10-01","lendDate":"","expectReturnDate":"","flag":1},{"bookId":"683487f0322c474ab5c5b8b57661b94e","bookName":"测试","bookTypeName":"军事","author":"王五","press":"北京出版社","publishDate":"2018-07-18","lendDate":"","expectReturnDate":"","flag":1},{"bookId":"7723d0b8aac24094b3dddf8b46aaba6e","bookName":"从你的全世界路过","bookTypeName":"科幻","author":"未知","press":"长春出版社","publishDate":"2018-07-10","lendDate":"2018-10-26","expectReturnDate":"2018-11-11","flag":0},{"bookId":"80f0a58386b44ed6a6e1a3dd3a9ec8ba","bookName":"测试2","bookTypeName":"科技","author":"赵六","press":"重庆出版社","publishDate":"2018-07-10","lendDate":"2018-10-09","expectReturnDate":"2018-10-23","flag":0},{"bookId":"b9290c8fcf7644278914c1635977de92","bookName":"水浒传","bookTypeName":"军事","author":"田七","press":"党建读物出版社","publishDate":"2018-05-07","lendDate":"2018-07-17","expectReturnDate":"2018-08-03","flag":0},{"bookId":"baeff19dead44329bd67ccad91c34cb1","bookName":"红楼梦","bookTypeName":"科技","author":"佚名","press":"法律出版社","publishDate":"2018-06-11","lendDate":"2018-07-10","expectReturnDate":"2018-07-20","flag":0},{"bookId":"e3250003c5fa4002abfa8525198a2c75","bookName":"三国演义","bookTypeName":"农业","author":"第八","press":"湖南人民出版社","publishDate":"2018-05-07","lendDate":"2018-07-18","expectReturnDate":"2018-09-06","flag":0}]
     */

    private int total;
    private List<DataListBean> dataList;

    public int getTotal() {
        return total;
    }

    public void setTotal(int total) {
        this.total = total;
    }

    public List<DataListBean> getDataList() {
        return dataList;
    }

    public void setDataList(List<DataListBean> dataList) {
        this.dataList = dataList;
    }

    public static class DataListBean {
        /**
         * bookId : 52821ca7f6ac40b9b4663813b384632f
         * bookName : 西游记
         * bookTypeName : 农业
         * author : 李四
         * press : 安徽人民出版社
         * publishDate : 2018-07-17
         * lendDate : 2018-07-10
         * expectReturnDate : 2018-07-18
         * flag : 0
         */

        private String bookId;
        private String bookName;
        private String bookTypeName;
        private String author;
        private String press;
        private String publishDate;
        private String lendDate;
        private String expectReturnDate;
        private int flag;

        public String getBookId() {
            return bookId;
        }

        public void setBookId(String bookId) {
            this.bookId = bookId;
        }

        public String getBookName() {
            return bookName;
        }

        public void setBookName(String bookName) {
            this.bookName = bookName;
        }

        public String getBookTypeName() {
            return bookTypeName;
        }

        public void setBookTypeName(String bookTypeName) {
            this.bookTypeName = bookTypeName;
        }

        public String getAuthor() {
            return author;
        }

        public void setAuthor(String author) {
            this.author = author;
        }

        public String getPress() {
            return press;
        }

        public void setPress(String press) {
            this.press = press;
        }

        public String getPublishDate() {
            return publishDate;
        }

        public void setPublishDate(String publishDate) {
            this.publishDate = publishDate;
        }

        public String getLendDate() {
            return lendDate;
        }

        public void setLendDate(String lendDate) {
            this.lendDate = lendDate;
        }

        public String getExpectReturnDate() {
            return expectReturnDate;
        }

        public void setExpectReturnDate(String expectReturnDate) {
            this.expectReturnDate = expectReturnDate;
        }

        public int getFlag() {
            return flag;
        }

        public void setFlag(int flag) {
            this.flag = flag;
        }
    }
}
