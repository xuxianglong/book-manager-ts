package com.dianxun.bookmanager.adapter.provider;

import com.chad.library.adapter.base.BaseViewHolder;
import com.chad.library.adapter.base.provider.BaseItemProvider;
import com.dianxun.bookmanager.R;
import com.dianxun.bookmanager.common.AppKeyManager;
import com.dianxun.bookmanager.entity.ReadingEntity;

/**
 * 标题
 */
public class ReadingOverdueProvider extends BaseItemProvider<ReadingEntity,BaseViewHolder> {

    @Override
    public int viewType() {
        return AppKeyManager.TYPE_READING_OVERDUE;
    }

    @Override
    public int layout() {
        return R.layout.item_reading_overdue;
    }

    @Override
    public void convert(BaseViewHolder helper, ReadingEntity data, int position) {
        helper.setText(R.id.tv_bookname, data.getBookName());
        helper.setText(R.id.tv_readdaynum, data.getReadDayNum() + "");
        helper.setText(R.id.tv_booktypename, data.getBookTypeName());
        helper.setText(R.id.tv_author, data.getAuthor());
        helper.setText(R.id.tv_press, data.getPress());
        helper.setText(R.id.tv_publishdate, data.getPublishDate());
        helper.setText(R.id.tv_lenddate, data.getLendDate());
        int expectReturnDayNum = data.getExpectReturnDayNum();
        if (expectReturnDayNum > 0) {
            helper.setText(R.id.tv_expectreturndaynum,  "还剩" + expectReturnDayNum + "天");
        } else if (expectReturnDayNum < 0) {
            helper.setText(R.id.tv_expectreturndaynum,  "已逾期" + Math.abs(expectReturnDayNum) + "天");
        } else {
            helper.setText(R.id.tv_expectreturndaynum,   "今天");
        }
    }

    @Override
    public void onClick(BaseViewHolder helper, ReadingEntity data, int position) {

    }

    @Override
    public boolean onLongClick(BaseViewHolder helper, ReadingEntity data, int position) {
        return true;
    }
}
