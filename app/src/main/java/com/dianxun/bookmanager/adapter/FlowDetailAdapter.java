package com.dianxun.bookmanager.adapter;

import com.chad.library.adapter.base.BaseQuickAdapter;
import com.chad.library.adapter.base.BaseViewHolder;
import com.dianxun.bookmanager.R;
import com.dianxun.bookmanager.entity.BookDetailEntity;

public class FlowDetailAdapter extends BaseQuickAdapter<BookDetailEntity.RecordDetailListBean, BaseViewHolder> {

    public FlowDetailAdapter() {
        super(R.layout.layout_flow_item, null);
    }

    @Override
    protected void convert(BaseViewHolder helper, BookDetailEntity.RecordDetailListBean item) {
        helper.setText(R.id.tv_username, item.getUserName());
        helper.setText(R.id.tv_lenddate, item.getLendDate());
        helper.setText(R.id.tv_returndate, item.getReturnDate());
    }
}